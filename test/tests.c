/*
 * tests.c
 *
 *  Created on: 25.05.2020
 *      Author: antonia
 */
#include "quelle.h"
#include "last.h"
#include "rs232.h"
#include "interfacer.h"
#include "tests.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

struct variables_read *read_variables_data;
struct variables_set reference_struct[]={
		{0,5.0,4.9,5.1},
		{0,12,11.9,12.1},
		{0,24,23.9,24.1},
		{0,30,29.9,30.1},
		{1,0.1,0.09,0.11},
		{1,0.2,0.19,0.21},
		{1,0.0,0,0.02},
		{1,1,0.95,1.05},
		{1,2,1.9,2.1},
		{1,2.9,2.8,3},
		{1,3,2.9,3.1},
		{2,3,2.45,2.55},
		{3,3,2.0,2.2},
		{3,1,0.0,0.03}
};

void Spannungstest_long(int power_supply_volt){
	printf("Spannungstest:\n\n");
	Output_OFF();
	Input_OFF();
	Quelle_Voltage_set(power_supply_volt);
	Output_ON();
	Last_CC_mode_set();
	Last_Strom_set(0);
	Input_ON();
	int size = 100;

	int current_id=0;
	double currents[]={0,1.0,2.0,2.9};
	int voltage_id;
	int voltage_output[VOLTAGES_TO_MEASURE]={5,12,24,30};
	for(voltage_id=0;voltage_id<VOLTAGES_TO_MEASURE;voltage_id++){
		for(current_id=0;current_id<CURRENTS_TO_MEASURE;current_id++){
			Spannungstest_data_set(voltage_output[voltage_id],currents[current_id]);
			sleep(1);
			if(output==0){
				sleep(20);
			}else{
				measurement_measure(voltage_output[voltage_id],currents[current_id],size);
			}
		}
	}
}

void Stromtest_long(int power_supply_volt){
	printf("Stromtest:\n\n");
	Input_OFF();
	Output_OFF();
	Quelle_Voltage_set(power_supply_volt);
	Last_CV_mode_set();
	Last_Spannung_set(6);
	Output_ON();
	usleep(1000);
	Input_ON();
	int size = 100;

	int current_id=0;
	double currents[]={0.2,1.0,2.0,3.0};
	int voltage_id;
	double voltage_output[VOLTAGES_TO_MEASURE]={5.1,12,24,30};
	int voltage_output_2[]={5,10,15,20,25,30};
	for(voltage_id=0;voltage_id<VOLTAGES_TO_MEASURE;voltage_id++){
		for(current_id=0;current_id<CURRENTS_TO_MEASURE;current_id++){
			Stromtest_data_set(30,voltage_output[voltage_id],currents[current_id]);
			sleep(1);
			measurement_measure(voltage_output[voltage_id],currents[current_id],size);
		}
	}

	for(voltage_id=0;voltage_id<5;voltage_id++){
		Stromtest_data_set(voltage_output_2[voltage_id],voltage_output_2[voltage_id+1],1.0);
		sleep(1);
		measurement_measure(voltage_output_2[voltage_id],1.0,size);
	}
}

void measurement_measure(int volt, double curr, int size){
	int type = 1;
	if(volt == 30 && curr == 3){
		type = 3;
	}else if(volt == 24 && curr == 3){
		type = 2;
	}else if(volt == 10 || volt == 15 || volt == 20 || volt == 25){
		type = 3;
	}
	struct variable_arrays *array;
	array=malloc(sizeof(var_read_t));
	array->voltage_array=malloc(sizeof(double*) * (size + 1));
	array->curr_array=malloc(sizeof(double*) * (size + 1));
	array->voltage_last_array=malloc(sizeof(double*) * (size + 1));
	array->curr_last_array=malloc(sizeof(double*) * (size + 1));
	for(int i = 0; i < size; i++){
		if(voltage_last == 0){
			printf("   !!!ERROR!!!\n");
			printf("   Measured 0V at load!\n");
			Input_OFF();
			Last_Strom_set(0);
			printf("FAILED: Test failed for Voltage %i and Current %f\n\n", volt, curr);
			sleep(2);
			Input_ON();
			return;
		}
		array->voltage_array[i]= voltage_read;
		array->curr_array[i] = curr_read;
		array->voltage_last_array[i]= voltage_last;
		array->curr_last_array[i]= curr_last;
		usleep(100000);
	}
	printf("Status for Voltage %i and Current %f\n", volt, curr);
	printf("Voltage Netzteil:\n");
	data_statistic(array->voltage_array,size,0,volt);
	printf("Current Netzteil:\n");
	data_statistic(array->curr_array,size,type,curr);
	printf("Voltage Last:\n");
	data_statistic(array->voltage_last_array,size,0,volt);
	printf("Current Last:\n");
	data_statistic(array->curr_last_array,size,type,curr);
	printf("\n");
	free(array);
}

void Spannungstest_data_set(int maxvolt, double loadcurr){
	variables_window_update(1,maxvolt); //Netzteil Spannung
	//Last_Strom_set(loadcurr);            //Last Strom
	current_stepwise_set(loadcurr);
}

void Spannungstest_data_set_directly(int maxvolt, double loadcurr){
	variables_window_update(1,maxvolt); //Netzteil Spannung
	Last_Strom_set(loadcurr);            //Last Strom
	//current_stepwise_set(loadcurr);
}

void Stromtest_data_set(float maxvolt, float loadvolt, double maxcurr){
	variables_window_update(1,maxvolt); //Netzteil Spannung
	Last_Spannung_set(loadvolt);         //Last Spannung
	//voltage_stepwise_set(loadvolt);
	variables_window_update(3,maxcurr); //Netzteil Strom
}

void data_statistic(float *data, int size, int type, double value){
	struct variables_read *mydd;
	int len = sizeof(mydd);
	mydd=calloc(sizeof(var_read_t),len);
	mydd->type = type;
	mydd->value_set = value;
	float sum = 0.0, mean, SD = 0.0;
	float minimum = data[0];
	float maximum = data[0];
	sum = sum + data[0];
	for (int i = 1; i < size; i++){
		minimum = min(minimum, data[i]);
		maximum = max(maximum, data[i]);
		sum += data[i];
	}
	mean = sum / size;
	for (int i = 0; i < size; ++i){
		SD += pow(data[i] - mean, 2);
	}
	SD = sqrt(SD / size);
	printf("Data: Min %f Max %f Mean %f SD %f\n", minimum, maximum, mean, SD);
	mydd->max = maximum;
	mydd->min = minimum;
	mydd->mean = mean;
	mydd->SD = SD;
	//read_variables_data=mydd;
	test_my_data(mydd);
	free(data);
}

void test_my_data(struct variables_read *variables_measured){
	struct variables_read *mydd;
	int len = sizeof(variables_measured);
	mydd=calloc(sizeof(var_read_t),len);
	mydd=variables_measured;
	for(int i=0; i<14; i++){
		//printf("measured:%f type:%i, reference:%f type:%i\n", mydd->value_set, mydd->type, reference_struct[i].value_set, reference_struct[i].type);
		if(mydd->value_set == reference_struct[i].value_set && mydd->type == 0 && reference_struct[i].type == 0){
			if(mydd->min>=reference_struct[i].min && mydd->max<=reference_struct[i].max){
				printf("PASSED: Voltage measurement PASSED.\n");
			}else{
				printf("FAILED: Voltage measurement FAILED.\n");
			}
		}else if(mydd->value_set == reference_struct[i].value_set && mydd->type == 1 && reference_struct[i].type == 1){
			if(mydd->min>=reference_struct[i].min && mydd->max<=reference_struct[i].max){
				printf("PASSED: Current measurement PASSED.\n");
			}else{
				printf("FAILED: Current measurement FAILED.\n");
			}
		}else if(mydd->value_set == reference_struct[i].value_set && mydd->type == 2 && reference_struct[i].type == 2){
			if(mydd->min>=reference_struct[i].min && mydd->max<=reference_struct[i].max){
				printf("PASSED: Current measurement PASSED.\n");
			}else{
				printf("FAILED: Current measurement FAILED.\n");
			}
		}else if(mydd->value_set == reference_struct[i].value_set && mydd->type == 3 && reference_struct[i].type == 3){
			if(mydd->min>=reference_struct[i].min && mydd->max<=reference_struct[i].max){
				printf("PASSED: Current measurement PASSED.\n");
			}else{
				printf("FAILED: Current measurement FAILED.\n");
			}
		}
	}
	free(variables_measured);
}

bool TestRange (double numberToCheck, double bottom, double top){
	return (numberToCheck >= bottom && numberToCheck <= top);
}

float max(float num1, float num2){
    return (num1 > num2 ) ? num1 : num2;
}

float min(float num1, float num2){
    return (num1 > num2 ) ? num2 : num1;
}

void  short_circuit(int power_supply_volt){
	printf("Short circuit test for CC mode:\n\n");
	Quelle_Voltage_set(power_supply_volt);
	Output_ON();
	Last_CC_mode_set();
	Last_Strom_set(0);
	Input_ON();
	usleep(1000);
	int current_id=0;
	double currents[]={0,1.0,2.0,2.9};
	int voltage_id;
	int voltage_output[VOLTAGES_TO_MEASURE]={5,12,24,30};
	for(voltage_id=0;voltage_id<VOLTAGES_TO_MEASURE;voltage_id++){
		for(current_id=0;current_id<CURRENTS_TO_MEASURE;current_id++){
			Spannungstest_data_set(voltage_output[voltage_id],currents[current_id]);
			sleep(1);
			if(output==0){
				printf("FAILED: Short circuit FAILED for voltage:%i and current:%f\n", voltage_output[voltage_id], currents[current_id]);
				sleep(20);
			}else{
				Last_short_circuit_ON();
				sleep(2);
				if(output==0){
					printf("FAILED: Short circuit FAILED for voltage:%i and current:%f\n", voltage_output[voltage_id], currents[current_id]);
					Last_short_circuit_OFF();
					sleep(20);
				}else{
					printf("PASSED: Short circuit PASSED for voltage:%i and current:%f\n", voltage_output[voltage_id], currents[current_id]);
					Last_short_circuit_OFF();
				}
				sleep(1);
			}
		}
	}

	printf("\n\n");
	printf("Short circuit test for CV mode:\n\n");
	Last_CV_mode_set();
	Input_ON();
	sleep(1);
	int current_id_cv=0;
	double currents_cv[]={0.2,1.0,2.0,3.0};
	int voltage_id_cv;
	double voltage_output_cv[VOLTAGES_TO_MEASURE]={5.1,12,24,30};
	int voltage_output_2_cv[]={5,10,15,20,25,30};
	for(voltage_id_cv=0;voltage_id_cv<VOLTAGES_TO_MEASURE;voltage_id_cv++){
		for(current_id_cv=0;current_id_cv<CURRENTS_TO_MEASURE;current_id_cv++){
			Stromtest_data_set(30,voltage_output_cv[voltage_id_cv],currents_cv[current_id_cv]);
			sleep(1);
			if(output==0){
				printf("FAILED: Short circuit FAILED for voltage:%f and current:%f\n", voltage_output_cv[voltage_id_cv], currents_cv[current_id_cv]);
				sleep(20);
			}else{
				Last_short_circuit_ON();
				sleep(2);
				if(output==0){
					printf("FAILED: Short circuit FAILED for voltage:%f and current:%f\n", voltage_output_cv[voltage_id_cv], currents_cv[current_id_cv]);
					Last_short_circuit_OFF();
					sleep(20);
				}else{
					printf("PASSED: Short circuit PASSED for voltage:%f and current:%f\n", voltage_output_cv[voltage_id_cv], currents_cv[current_id_cv]);
					Last_short_circuit_OFF();
				}
				sleep(1);
			}
		}
	}

	for(voltage_id_cv=0;voltage_id_cv<6;voltage_id_cv++){
		Stromtest_data_set(voltage_output_2_cv[voltage_id_cv],voltage_output_2_cv[voltage_id_cv+1],1.0);
		sleep(1);
		if(output==0){
			printf("FAILED: Short circuit FAILED for voltage:%i and current:1.0\n", voltage_output_2_cv[voltage_id_cv]);
			sleep(20);
		}else{
			Last_short_circuit_ON();
			sleep(2);
			if(output==0){
				printf("FAILED: Short circuit FAILED for voltage:%i and current:1.0\n", voltage_output_2_cv[voltage_id_cv]);
				Last_short_circuit_OFF();
				sleep(20);
			}else{
				printf("PASSED: Short circuit PASSED for voltage:%i and current:1.0\n", voltage_output_2_cv[voltage_id_cv]);
				Last_short_circuit_OFF();
			}
			sleep(1);
		}
	}
}

void voltage_sags_test(int power_supply_volt){
	printf("Voltage sag test for %i V:\n\n", power_supply_volt);
	Quelle_Voltage_set(power_supply_volt);
	Output_ON();
	Last_CC_mode_set();
	Last_Strom_set(0);
	Input_ON();
	printf("Voltage dips in CC mode\n\n");
	int id = 0;
	int sag_percent[] = {70,70};
	double sag_sec[] = {0.5,0.6};
	int current_id=0;
	double currents[]={0,1.0,2.0,2.9};
	int voltage_id;
	int voltage_output[VOLTAGES_TO_MEASURE]={5,12,24,30};
	for(voltage_id=0;voltage_id<VOLTAGES_TO_MEASURE;voltage_id++){
		for(current_id=0;current_id<CURRENTS_TO_MEASURE;current_id++){
			printf("Testing for voltage:%i and current:%f\n", voltage_output[voltage_id], currents[current_id]);
			for(id=0; id<2; id++){
				Spannungstest_data_set(voltage_output[voltage_id],currents[current_id]);
				sleep(3);
				Quelle_List_operation(power_supply_volt, sag_percent[id], sag_sec[id]);
				Quelle_Voltage_set(power_supply_volt);
			}
			printf("\n");
		}
	}

	sleep(2);
	printf("\n\n");
	printf("Voltage dips in CV mode\n\n");
	Input_OFF();
	Output_OFF();
	Quelle_Voltage_set(power_supply_volt);
	Last_CV_mode_set();
	Last_Spannung_set(6);
	Output_ON();
	usleep(1000);
	Input_ON();
	sleep(2);
	int current_id_cv=0;
	double currents_cv[]={0.2,1.0,2.0,3.0};
	int voltage_id_cv;
	double voltage_output_cv[VOLTAGES_TO_MEASURE]={5.1,12,24,30};
	int voltage_output_2_cv[]={5,10,15,20,25,30};
	for(voltage_id_cv=0;voltage_id_cv<VOLTAGES_TO_MEASURE;voltage_id_cv++){
		for(current_id_cv=0;current_id_cv<CURRENTS_TO_MEASURE;current_id_cv++){
			printf("Testing for voltage:%f and current:%f\n", voltage_output_cv[voltage_id_cv], currents_cv[current_id_cv]);
			for(id=0; id<2; id++){
				Stromtest_data_set(30,voltage_output_cv[voltage_id_cv],currents_cv[current_id_cv]);
				sleep(2);
				Quelle_List_operation(power_supply_volt, sag_percent[id], sag_sec[id]);
				Quelle_Voltage_set(power_supply_volt);
			}
			printf("\n");
		}
	}

	for(voltage_id_cv=0;voltage_id_cv<5;voltage_id_cv++){
		printf("Testing for voltage:%i and current:1.0\n", voltage_output_2_cv[voltage_id_cv]);
		for(id=0; id<2; id++){
			Stromtest_data_set(voltage_output_2_cv[voltage_id_cv],voltage_output_2_cv[voltage_id_cv+1],1.0);
			sleep(2);
			Quelle_List_operation(power_supply_volt, sag_percent[id], sag_sec[id]);
			Quelle_Voltage_set(power_supply_volt);
		}
		printf("\n");
	}
}

void changing_load_current(int power_supply_volt, double curr_1, double curr_2){
	printf("Changing current test for %i V:\n\n", power_supply_volt);
	Output_OFF();
	Input_OFF();
	Quelle_Voltage_set(power_supply_volt);
	Output_ON();
	Last_CC_mode_set();
	Last_Strom_set(0);
	Input_ON();
	int curr_id;
	int voltage_id;
	int voltage_output[VOLTAGES_TO_MEASURE]={5,12,24,30};
	for(curr_id=0;curr_id<29;curr_id++){
		for(voltage_id=0;voltage_id<VOLTAGES_TO_MEASURE;voltage_id++){
			Spannungstest_data_set_directly(voltage_output[voltage_id],curr_1);
			sleep(1);
			for(int i=0; i<2; i++){
				Spannungstest_data_set_directly(voltage_output[voltage_id],curr_1);
				for(int loop=0; loop <20; loop++){
					if(voltage_last < (voltage_output[voltage_id]-0.5)){
						printf("Test FAILED for load voltage %i and current between: %f and %f!!!\n", voltage_output[voltage_id], curr_1, curr_2);
						break;
					}
					usleep(100000);
				}

				Spannungstest_data_set_directly(voltage_output[voltage_id],curr_2);
				for(int loop_2=0; loop_2 <20; loop_2++){
					if(voltage_last < (voltage_output[voltage_id]-0.5)){
						printf("Test FAILED for load voltage %i and current between: %f and %f!!!\n", voltage_output[voltage_id], curr_1, curr_2);
						break;
					}
					usleep(100000);
				}
			}
		}
		curr_2=curr_2+0.1;
	}

}

void voltage_sag(int power_supply_volt, int percent, int millisec){
	int volt = (power_supply_volt*percent)/100;
	Quelle_Voltage_set(volt);
	usleep(millisec*1000);
}

void current_stepwise_set(double curr){
	for(int i=1; i<11; i++){
		Last_Strom_set((curr-1.0)+(0.1*i));
		usleep(200000);
	}
}

void voltage_stepwise_set(int volt){
	for(int i=1; i<6; i++){
		Last_Strom_set((volt-5)+i);
		usleep(100000);
	}
}
