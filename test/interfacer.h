#define MCU_TRACER_STARTBYTE 0xA5
#define PRINT_RS232_DATA 0

#define FUNC_NAME_LENGTH 30

#include <stdint.h>

extern float voltage_set;
extern float voltage_read;
extern float curr_set;
extern float curr_read;
extern int status;

typedef struct set_variables{
	char label[30];
	int type; //0 notused (terminator), 1 long, 2 float, 3 toogle, 4 unsigned long
	int rw;
	union{
		int32_t data_l;
		float data_f;
	};
} set_var_t;


typedef struct mcu_func{
	char name[FUNC_NAME_LENGTH];
	int id;
} mcu_func_t;

typedef struct set_single_var{
	uint16_t addr;
	uint32_t val;
} set_single_var_t;

//
int monitor_test_connection(int number, int baud);
void * monitor_recieve_thread(void);
void monitor_recieve_terminate(void);
void monitor_master_allow_decoding(void);
void monitor_master_decode_string(unsigned char* buf, int len);
void monitor_master_req_init(void);
void monitor_master_req_data(void);
void monitor_master_write_var(uint16_t num, int32_t val);
void monitor_master_func_data(void);
void monitor_master_func_exec(uint8_t id);
void monitor_master_emergency(void);
void monitor_master_req_frequently_enable(void);
void monitor_master_req_frequently_disable(void);
void monitor_rs232_disconnect(void);
void variables_window_update_vars(uint32_t *datastream);
void variables_window_update(int num, float val);
void Netzteil_connect(void);
