/*
 * quelle.h
 *
 *  Created on: 05.05.2020
 *      Author: antonia
 */

#ifndef QUELLE_H_
#define QUELLE_H_

extern int quelle;
extern int output;

void Quelle_connect(void);
void Quelle_disconnect(void);

void Quelle_Voltage_set(int a);

void Quelle_List_operation(int power_supply_volt, int percent, double sec);

void Output_ON(void);
void Output_OFF(void);

#endif
