/*
 * quelle.c
 *
 *  Created on: 05.05.2020
 *      Author: antonia
 */

#include "unified_header.h"
#include "last.h"

int quelle;
int output=0;

pthread_mutex_t plock_rs232_quelle= PTHREAD_MUTEX_INITIALIZER;

void Quelle_connect(void){
	quelle=open("/dev/ttyUSB0",O_RDWR);

	struct termios SerialPortSettings;

	tcgetattr(quelle, &SerialPortSettings);
	cfsetispeed(&SerialPortSettings,B9600);
	cfsetospeed(&SerialPortSettings,B9600);

	SerialPortSettings.c_cflag &= ~PARENB;
	SerialPortSettings.c_cflag &= ~CSTOPB;
	SerialPortSettings.c_cflag &= ~CSIZE;
	SerialPortSettings.c_cflag |=  CS8;

	if(quelle>0){
		printf("Connected to Quelle.\n");
		pthread_mutex_lock(&plock_rs232_quelle);
		dprintf(quelle,"SYST:REM\n");
		pthread_mutex_unlock(&plock_rs232_quelle);
	}
	else{
		printf("Unable to connect to Quelle.\n");
	}
}

void Quelle_List_operation(int power_supply_volt, int percent, double sec){
	int id = 0;
	int volt = (power_supply_volt*percent)/100;
	//double time = millisec/1000;
	int list_voltage[3]={power_supply_volt,volt,power_supply_volt};
	double list_dwell[3]={3,sec,0.5};
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"LIST:STEP:COUN 3\n");
	pthread_mutex_unlock(&plock_rs232_quelle);
	usleep(100000);
	for(id=0; id<3; id++){
		pthread_mutex_lock(&plock_rs232_quelle);
		dprintf(quelle,"LIST:STEP:VOLT %i,%i\n", id, list_voltage[id]);
		pthread_mutex_unlock(&plock_rs232_quelle);
		usleep(100000);
		pthread_mutex_lock(&plock_rs232_quelle);
		dprintf(quelle,"LIST:STEP:SLOP %i,%i\n", id, 0);
		pthread_mutex_unlock(&plock_rs232_quelle);
		usleep(100000);
		pthread_mutex_lock(&plock_rs232_quelle);
		dprintf(quelle,"LIST:STEP:DWEL %i,%f\n", id, list_dwell[id]);
		pthread_mutex_unlock(&plock_rs232_quelle);
		usleep(100000);
	}
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"LIST:STAT ENAB\n");
	pthread_mutex_unlock(&plock_rs232_quelle);
	usleep(100000);
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"TRIG\n");
	pthread_mutex_unlock(&plock_rs232_quelle);
	sleep(4);
	if(voltage_last < 4 || output == 0){
		pthread_mutex_lock(&plock_rs232_quelle);
		dprintf(quelle,"LIST:STAT DIS\n");
		pthread_mutex_unlock(&plock_rs232_quelle);
		printf("FAILED: Voltage sag FAILED for %i percent and %f seconds\n",percent, sec);
		sleep(4);
		if(output == 0){
			sleep(25);
			for(int i = 0; i < 10; i++){
				if(output == 0 || voltage_last < 4){
					sleep(25);
				}else{
					return;
				}
			}
		}else{
			printf("   Measured 0V at load!\n");
			sleep(1);
		}

	}else{
		printf("PASSED: Voltage sag PASSED for %i percent and %f seconds\n",percent, sec);
		sleep(sec);
	}
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"LIST:STAT DIS\n");
	pthread_mutex_unlock(&plock_rs232_quelle);
}

void Quelle_disconnect(void){
	close(quelle);
	printf("Disconnected.\n");
}

void Quelle_Voltage_set(int a){
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"VOLT:IMM %d\n",a);
	pthread_mutex_unlock(&plock_rs232_quelle);
}

void Output_ON(void){
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"OUTP 1\n");
	pthread_mutex_unlock(&plock_rs232_quelle);
	output=1;
	sleep(1);
}

void Output_OFF(void){
	pthread_mutex_lock(&plock_rs232_quelle);
	dprintf(quelle,"OUTP 0\n");
	pthread_mutex_unlock(&plock_rs232_quelle);
	output=0;
}

