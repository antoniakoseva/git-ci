#include "unified_header.h"
#include "tests.h"
#include "quelle.h"
#include "last.h"

int main(void){
	Quelle_connect();
	Output_ON();
	Last_connect();
	Last_req_frequently_enable();
	usleep(1000);

	Netzteil_connect();

	//Spannungstest_long(230);

	//sleep(1);

	//Stromtest_long(230);

	//sleep(1);
	//short_circuit(230);

	//voltage_sags_test(230);
	changing_load_current(230,0,0.1);

	Input_OFF();
	Output_OFF();
	Last_disconnect();
	Quelle_disconnect();
}
