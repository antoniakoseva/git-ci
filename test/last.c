/*
 * last.c
 *
 *  Created on: 06.05.2020
 *      Author: antonia
 */
#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "quelle.h"

int last;
float voltage_last;
float curr_last;

int connected = 0;
int input = 0;
int short_c = 0;

pthread_mutex_t plock_rs232_last= PTHREAD_MUTEX_INITIALIZER;
pthread_t th_req_last;

void Last_connect(void){
	last=open("/dev/ttyUSB2",O_RDWR);

	struct termios SerialPortSettings;

	tcgetattr(last, &SerialPortSettings);
	cfsetispeed(&SerialPortSettings,B9600);
	cfsetospeed(&SerialPortSettings,B9600);

	SerialPortSettings.c_cflag &= ~PARENB;
	SerialPortSettings.c_cflag &= ~CSTOPB;
	SerialPortSettings.c_cflag &= ~CSIZE;
	SerialPortSettings.c_cflag |=  CS8;

	if(last>0){
		printf("Connected to Last.\n");
		pthread_mutex_lock(&plock_rs232_last);
		dprintf(last,"SYST:REM\n");
		dprintf(last,"SYST:SENS 1\n");
		pthread_mutex_unlock(&plock_rs232_last);
		connected = 1;
	}
	else{
		printf("Unable to connect to Last.\n");
	}
}

void Last_disconnect(void){
	close(last);
	printf("Disconnected.\n");
	connected = 0;
}

void Last_CC_mode_set(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"MODE CURR\n");
	pthread_mutex_unlock(&plock_rs232_last);
}

void Last_CV_mode_set(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"MODE VOLT\n");
	pthread_mutex_unlock(&plock_rs232_last);
}

void Last_Strom_set(double a){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"CURR:IMM %f\n",a);
	pthread_mutex_unlock(&plock_rs232_last);
}

void Last_Spannung_set(double a){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"VOLT:IMM %f\n",a);
	pthread_mutex_unlock(&plock_rs232_last);
}

void Input_ON(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"INP 1\n");
	pthread_mutex_unlock(&plock_rs232_last);
	input = 1;
	//sleep(1);
}

void Input_OFF(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"INP 0\n");
	pthread_mutex_unlock(&plock_rs232_last);
	input = 0;
}

void Last_short_circuit_ON(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"INP:SHOR 1\n");
	pthread_mutex_unlock(&plock_rs232_last);
	short_c = 1;
}

void Last_short_circuit_OFF(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"INP:SHOR 0\n");
	pthread_mutex_unlock(&plock_rs232_last);
	short_c = 0;
}

void Last_measure_V(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"MEAS:VOLT?\n");
	char a[20];
	read(last, a, sizeof(a));
	pthread_mutex_unlock(&plock_rs232_last);
	const char deli[] = "\n";
	char *token;
	token = strtok(a, deli);
	voltage_last = atof(token);
	//printf("Spannung: %f\n", voltage_last);

}

void Last_measure_I(void){
	pthread_mutex_lock(&plock_rs232_last);
	dprintf(last,"MEAS:CURR?\n");
	char a[20];
	read(last, a, sizeof(a));
	pthread_mutex_unlock(&plock_rs232_last);
	const char deli[] = "\n";
	char *token;
	token = strtok(a, deli);
	curr_last = atof(token);
	//printf("Strom: %f\n", curr_last);
	//printf("data: %f\n", curr_last);

}

void handle_error(void){
	Input_OFF();
	sleep(1);
	Last_measure_V();
	usleep(100000);
	if(voltage_last == 0 && short_c == 0){
		printf("   Power adapter turned off!\n");
		Output_OFF();
		Last_Strom_set(0);
		sleep(20);
		Output_ON();
		Last_measure_V();
		usleep(100000);
		if(voltage_last == 0 || voltage_last < 4){
			printf("   Power adapter turned off!\n");
			Output_OFF();
			Last_Strom_set(0);
			sleep(20);
			Output_ON();
		}
	}
	Input_ON();
}

void Last_req_frequently(void){
	while(connected == 1){
		usleep(100000);
		Last_measure_V();
		Last_measure_I();
		if(input == 1 && voltage_last == 0){
			handle_error();
			sleep(2);
		}
	}
}

void Last_req_frequently_enable(void){
	pthread_create(&th_req_last,NULL,(void * (*)(void *))Last_req_frequently,NULL);
}
