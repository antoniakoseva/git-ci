/*
 * tests.h
 *
 *  Created on: 25.05.2020
 *      Author: antonia
 */

#ifndef TESTS_H_
#define TESTS_H_

#include <stdbool.h>

#define VOLTAGES_TO_MEASURE 4
#define CURRENTS_TO_MEASURE 4
#define SIZE 100

typedef struct variables_read{
	int type;
	double value_set;
	double min;
	double max;
	double mean;
	double SD;
} var_read_t;

typedef struct variables_set{
	int type;
	double value_set;
	double min;
	double max;
} var_set_t;


typedef struct variable_arrays{
	float *voltage_array;
	float *curr_array;
	float *voltage_last_array;
	float *curr_last_array;
}var_arr_t;

void Spannungstest_long(int power_supply_volt);
void Stromtest_long(int power_supply_volt);
void data_statistic(float *data, int size, int type, double value);
void measurement_measure(int volt, double curr, int size);
void Spannungstest_data_set(int maxvolt, double loadcurr);
void Spannungstest_data_set_directly(int maxvolt, double loadcurr);
void Stromtest_data_set(float maxvolt, float loadvolt, double maxcurr);
void test_my_data(struct variables_read *variables_measured);
float max(float num1, float num2);
float min(float num1, float num2);
bool TestRange (double numberToCheck, double bottom, double top);
void short_circuit(int power_supply_volt);
void voltage_sags_test(int power_supply_volt);
void voltage_sag(int power_supply_volt, int percent, int millisec);
void current_stepwise_set(double curr);
void voltage_stepwise_set(int volt);
void changing_load_current(int power_supply_volt, double curr_1, double curr_2);

#endif /* TESTS_H_ */
