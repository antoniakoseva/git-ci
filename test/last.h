/*
 * last.h
 *
 *  Created on: 06.05.2020
 *      Author: antonia
 */

#ifndef LAST_H_
#define LAST_H_

#define RESTART_TIME 20

extern int last;
extern float voltage_last;
extern float curr_last;
extern int input;

void Last_connect(void);
void Last_disconnect(void);

void Last_CC_mode_set(void);
void Last_CV_mode_set(void);

void Last_Strom_set(double a);
void Last_Spannung_set(double a);

void Input_ON(void);
void Input_OFF(void);

void Last_short_circuit_ON(void);
void Last_short_circuit_OFF(void);

void Last_measure_V(void);
void Last_measure_I(void);

void Last_req_frequently(void);
void Last_req_frequently_enable(void);

void handle_error(void);

#endif
